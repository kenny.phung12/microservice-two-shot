# Generated by Django 4.0.3 on 2023-06-03 00:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shoe',
            name='brand',
        ),
        migrations.RemoveField(
            model_name='shoe',
            name='name',
        ),
        migrations.AddField(
            model_name='shoe',
            name='manufacturer',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='shoe',
            name='model_name',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='shoes', to='shoes_rest.binvo'),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='color',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
