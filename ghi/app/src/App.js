import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import CreateHat from './CreateHat';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe';
import { useEffect, useState } from 'react';


function App() {
const [hats, setHats] = useState([]);

const getHats = async () => {
  const hatsResponse = await fetch('http://localhost:8090/api/hats/')

  if (hatsResponse.ok) {
    const hatData = await hatsResponse.json()

    setHats(hatData.hats)
  }
}

  useEffect(() => {
    getHats();
  }, [])

const [shoes, setShoes] = useState([]);

const getShoes = async () => {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/')

  if (shoesResponse.ok) {
    const shoeData = await shoesResponse.json()

    setShoes(shoeData.shoes)
    }
  }

    useEffect(() => {
      getShoes();
    }, [])



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={hats} getHats={getHats}/>} />
          <Route path="/create/hat" element={<CreateHat getHats={getHats}/>} />
          <Route path="/shoes" element={<ShoesList shoes={shoes} getShoes={getShoes}/>} />
          <Route path="/create/shoe" element={<CreateShoe getShoes={getShoes} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
